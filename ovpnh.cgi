#!/bin/sh

BASEDIR="$(dirname "$(realpath "$0")")"
OVPNS_DIR="$BASEDIR/ovpns"
CREDENTIALS="$BASEDIR/credentials.txt" # password under username
OVPN_LOG="$BASEDIR/log.txt"
DATALIST=true
ENABLE_ROUTING=true
SUDO='sudo'

OVPN="${QUERY_STRING##*=}"

[ -n "$OVPN" ] && [ -f "$OVPNS_DIR/$OVPN" ] && {
    [ -n "$(pidof openvpn)" ] && "$SUDO" pkill openvpn
    sleep 2

    [ "$ENABLE_ROUTING" = true ] && [ "$(cat /proc/sys/net/ipv4/ip_forward)" -eq 0 ] && {
        "$SUDO" sh -c 'echo 1 > /proc/sys/net/ipv4/ip_forward'
        "$SUDO" iptables -t nat -A POSTROUTING -o tun0 -j MASQUERADE
        "$SUDO" iptables -A FORWARD -i tun0 -o eth0 -m state --state RELATED,ESTABLISHED -j ACCEPT
        "$SUDO" iptables -A FORWARD -i eth0 -o tun0 -j ACCEPT
    }

    (
    cd "$BASEDIR" || exit 1
    cat "$OVPNS_DIR/$OVPN"
    cat << EOF
auth-user-pass "$CREDENTIALS"
EOF
    ) | "$SUDO" openvpn /dev/stdin > "$OVPN_LOG" 2>&1 & sleep 1
}

render_form() {
    echo '<form>'
    if [ "$DATALIST" = true ]; then
        cat << EOF
        <input type="text" name="ovpn" list="ovpns" placeholder="example.ovpn" value="$OVPN">
        <datalist id="ovpns">
EOF
        find "$OVPNS_DIR" -name '*.ovpn' | while read -r line; do
            echo "            <option value='${line##*/}'>"
        done
        echo '        </datalist>'
    else
        cat << EOF
        <input type="text" name="ovpn" placeholder="example.ovpn" value="$OVPN">
EOF
    fi
    cat << EOF
        <input type="submit" value="Connect">
    </form>
EOF
}

cat << EOF
Content-type: text/html; charset=utf-8

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OpenVPN Helper</title>
    <style>
body {
    max-width: 40em;
    margin: 1em auto;
    padding: 0 1em;
    font-family: sans-serif;
}

.log {
    overflow: scroll;
    max-height: 20em;
}
    </style>
</head>
<body>
    <h1><a href="./$(basename "$0")" style="text-decoration: none; color: black;">OpenVPN Helper</a></h1>
    <h3>Gateway: <code>$REMOTE_ADDR</code></h3>
    <h3>Routing: <code>$(cat /proc/sys/net/ipv4/ip_forward)</code></h3>
    <h3>OpenVPN: <code>$(pidof openvpn || echo Disconnected)</code></h3>
    $(render_form)
    <h3>OpenVPN Log:</h3>
    <pre class="log"><code>$([ -f "$OVPN_LOG" ] && cat "$OVPN_LOG")</code></pre>
</body>
</html>
EOF
