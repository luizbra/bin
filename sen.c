#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/random.h>
#include "monocypher.h" /* version 3.1.3 */

int main(int argc, char *argv[]) {
	if (argc < 4) {
		fprintf(stderr, "usage: %s [-ed] [input] [output]\n", argv[0]);
		return 1;
	}

	FILE *fp;
	int64_t fsize;
	uint8_t *buffer;
	char password[128];
	uint8_t key[32];
	uint8_t salt[16];
	uint8_t nonce[24] = {0}; /* see: https://old.reddit.com/r/cryptography/comments/ukjwqy/simple_file_encryption/i7qj4j3/ */
	uint8_t mac[16];
	const uint32_t nb_blocks = 100000;
	const uint32_t nb_iterations = 3;
	void *work_area;

	fp = fopen(argv[2], "rb");
	if (fp == NULL) {
		fprintf(stderr, "unable to open input\n");
		return 1;
	}
	fseek(fp, 0L, SEEK_END);
	fsize = ftell(fp);
	fseek(fp, 0L, SEEK_SET);
	buffer = malloc(fsize);
	work_area = malloc(nb_blocks * 1024);
	if (buffer == NULL || work_area == NULL) {
		fprintf(stderr, "unable to allocate memory\n");
		fclose(fp);
		return 1;
	}
	fread(buffer, fsize, 1, fp);
	fclose(fp);
	fp = fopen(argv[3], "wb");
	if (fp == NULL) {
		fprintf(stderr, "unable to open output\n");
		return 1;
	}

	printf("password: ");
	fgets(password, sizeof (password), stdin);
	password[strcspn(password, "\n")] = 0;

	if (strcmp("-e", argv[1]) == 0) {
		getrandom(&salt, sizeof (salt), 0);
		crypto_argon2i(key, sizeof (key), work_area, nb_blocks, nb_iterations, (uint8_t *) password, strlen(password), salt, sizeof (salt));
		crypto_lock(mac, buffer, key, nonce, buffer, fsize);
		fwrite(salt, sizeof (salt), 1, fp);
		fwrite(mac, sizeof (mac), 1, fp);
		fwrite(buffer, fsize, 1, fp);
	} else if (strcmp("-d", argv[1]) == 0) {
		const uint8_t header_size = sizeof (salt) + sizeof (mac);
		if (fsize < header_size) {
			fprintf(stderr, "invalid input format\n");
			fclose(fp);
			crypto_wipe(password, sizeof (password));
			crypto_wipe(buffer, fsize);
			free(work_area);
			free(buffer);
			return 1;
		}
		memcpy(salt, buffer, sizeof (salt));
		memcpy(mac, &buffer[sizeof (salt)], sizeof (mac));
		crypto_argon2i(key, sizeof (key), work_area, nb_blocks, nb_iterations, (uint8_t *) password, strlen(password), salt, sizeof (salt));
		if (crypto_unlock(&buffer[header_size], key, nonce, mac, &buffer[header_size], fsize - header_size)) fprintf(stderr, "decryption failed\n");
		fwrite(&buffer[header_size], fsize - header_size, 1, fp);
	} else {
		fprintf(stderr, "bad usage\n");
		fclose(fp);
		crypto_wipe(password, sizeof (password));
		crypto_wipe(buffer, fsize);
		free(work_area);
		free(buffer);
		return 1;
	}

	fclose(fp);
	crypto_wipe(key, sizeof (key));
	crypto_wipe(password, sizeof (password));
	crypto_wipe(buffer, fsize);
	free(work_area);
	free(buffer);
	return 0;
}
